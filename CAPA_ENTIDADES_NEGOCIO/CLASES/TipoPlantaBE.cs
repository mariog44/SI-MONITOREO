﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPA_ENTIDADES_NEGOCIO.CLASES
{
    public class TipoPlantaBE
    {

        public int Id_Tipo_Planta { get; set; }

        public string Nombre_Tipo_Planta { get; set; }

    }
}
