﻿namespace CAPA_ENTIDADES_NEGOCIO.CLASES
{
    public class PlantaBE
    {

        public int Id_Planta { get; set; }

        public string Nombre_Planta { get; set; }

        public double Diametro_Tallo_Planta { get; set; }

        public double Tiempo_Cultivo_Planta { get; set; }

        public int Cantidad_Hojas_Planta { get; set; }

        public string Altura_Planta { get; set; }

        public int Id_Usuario_Planta { get; set; }

        public string Estado_Planta { get; set; }

        public string Tipo_Planta_Planta { get; set; }

        public string Unidad_Medida_Planta_Planta { get; set; }

        public string Unidad_Tiempo_Planta_Planta { get; set; }


    }
}
